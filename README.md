deep-file-search is a case-insensitive package which opens and scours all files in a given directory for a given search term and logs, returns or writes a new file containing human-readable and machine-parsable results indicating the locations of all instances of the particular search terms

Command Line Instruction:
0. Prepare arguments: place arguments in '' or "" (minus the <> below); Parent search dir & output dir need trailing "\"; If selecting output dir, encodeType and saveFile must be declared, even if defaults maintained ("utf8" "true")
1. npx deep-file-search "<.\relative path to search dir\>" "<search term(s)>" "<encodeType(optional, default utf8)>" "<saveFile(optional, default true)>" "<.\outputDirectory(optional, must already exist, default outputs at terminal location)\>"
2. ???
3. Profit

Programmatic Usage:
1. npm i deep-file-search
2. let deepFileSearch = require('deep-file-search')
3. let deepFileSearchResultsObject = await deep-file-search.corre(string_searchDir, string_searchTerm, OPTIONAL_fileEncodeType, OPTIONAL_bool_outputFile, OPTIONAL_string_outputDir) 

In Programmatic Usage, the default setting is not to output any file search results to disk.

File Encode Type (default is utf8); Options Include:
ascii, base64, base64url, binary, hex, latin1, ucs-2, ucs2, utf-8, utf16le, utf8

Git Repo: https://gitlab.com/calexh/deep-file-search

Thank You for Using deep-file-search, written by C. Alex @ YHY/SPC LLC 
We Contract Internationally!  🇷🇺 - 🇲🇽 - 🇨🇿 - 🇫🇮 - 🇸🇪 - 🇩🇪 - 🇺🇸 
Visit YHY.fi & work with YHY/SPC LLC on your next project.