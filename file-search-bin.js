#!/usr/bin/env node
// Wir haben fs, process
var fs = require('fs')
const process = require('process')
// Kansio haettava
let parentDir = ''
// Ulostulo kansio
let outputDir = '.'
// Sana tai sanat mikä etsit
let kw = ''
// Encoding (muchas veces solamente utf8)
let encode = 'utf8'
// Tee muisto vai ei
let saving = true
// Сейчас
let d = new Date()



// Saca los argumentos (saca la bolsita guey)
let args = process.argv.slice(2)
// Параметеры в своих домы
if(args.length>0){parentDir = (args[0].split('"').length>=1 && args[0].split('"')[0].length==0) ? args[0].split('"')[1] : ((args[0].split("'").length>=1 && args[0].split("'")[0].length ==0) ? args[0].split("'")[1] : args[0])}
if(args.length>1){kw = (args[1].split('"').length>=1 && args[1].split('"')[0].length==0) ? args[1].split('"')[1] : ((args[1].split("'").length>=1 && args[1].split("'")[0].length ==0) ? args[1].split("'")[1] : args[1])}
if(args.length>2){encode = (args[2].split('"').length>=1 && args[2].split('"')[0].length==0) ? args[2].split('"')[1] : ((args[2].split("'").length>=1 && args[2].split("'")[0].length ==0) ? args[2].split("'")[1] : args[2])}
if(args.length>3){saving = !!JSON.parse(args[3].split('"').length>=1 && args[3].split('"')[0].length==0) ? args[3].split('"')[1] : ((args[3].split("'").length>=1 && args[3].split("'")[0].length ==0) ? args[3].split("'")[1] : args[3])}
if(args.length>4){outputDir = (args[4].split('"').length>=1 && args[4].split('"')[0].length==0) ? args[4].split('"')[1] : ((args[4].split("'").length>=1 && args[4].split("'")[0].length ==0) ? args[4].split("'")[1] : args[4])}



// Это делает работа
async function main(){
    try{
        // Koti uudet tieotille
        let löysinJotain = []
        let lkw = kw.toLowerCase()

        // Kaikki tiedot ovat täälä, jos ei, se tee Virhe
        if((args.length < 2 && kw.length==0 && parentDir.length==0) || args.length > 5){
            console.log('Package Missing Required Command-Line Argument\nPlease Include Source Parent Directory, Output Directory, and Keyword(s) in "Quotation" Marks.')
            console.log('Paquete No Tiene Todo Que Se Necesita\nPor Favor pon la carpeta pariente de los ficheros, una carpeta para la respuesta, y las palabra que quiere buscar en "quotaciones".')
            console.log('Pakettillä ei ole kaikki mikä se tarvii\nLaita ylempi kansio tiedostoja, vastauskansio , ja sana sä haluat etsiä "lainausmerkiksen sisällä".')
            console.log('В пакете отсутствует требуемый аргумент командной строки\n Пожалуйста, укажите исходный родительский каталог, выходной каталог и ключевые слова "в кавычках".')
            console.log('Paketet har inte allt det behöver\nSätta den övre mappen med filer, svarsmappen och ordet du vill söka efter "inuti citattecknet"')
            console.log('Das Paket enthält nicht alles, was es benötigt\nLegen Sie den oberen Ordner mit Dateien, den Antwortordner und das Wort, nach dem Sie suchen möchten, "in das Anführungszeichen"')
            return true
        }

        // Sana tarvii olla jotain, ei Null kai
        if(kw.length < 1){
            console.log('Please Enter a Keyword and Try Again')
            console.log('Escribe la palabra que busque y trata otra vez por favor')
            console.log('Kirjoittaa avainsanan että etsit ja yrita toisen kerra kiitos')
            console.log('Введите слово, которое вы ищете, и попробуйте еще раз, пожалуйста')
            console.log('Skriv ordet du letar efter och försök igen tack')
            console.log('Geben Sie das gesuchte Wort ein und versuchen Sie es erneut bitte')
            return true
        }

        // Nae kaikki tiedot ja hae ne listiin
        const filesInDir = []
        console.log('Inventorying files...')
        fs.readdirSync(parentDir, {recursive:true}).forEach(file => {if(!fs.lstatSync(parentDir+file).isDirectory()){filesInDir.push(file)}});
        console.log('Beginning File Inspection...')

        // Katelee sisälle kaikki naa juttuja, что там внутри
        for(let i=0; i < filesInDir.length; ++i){
            console.log('Inspecting: ', filesInDir[i])
            let currentdata = fs.readFileSync(`${parentDir}${filesInDir[i]}`, encode);
            let originalData = currentdata
            currentdata = currentdata.toLowerCase()
            let splitdoc = currentdata.split(lkw)
            if(splitdoc.length > 1){
                let contextIdx = 0
                let contextIdxIdx = 0
                // Keyword Found in Given File, Provide Location & Context
                let splitbyline = currentdata.split('\n')
                for(let j=0; j < splitbyline.length; ++j){
                    if(splitbyline[j].split(lkw).length > 1){
                        // found keyword on line j, line j could be a whole paragraph doe, and searchterm could be inside more than 1x
                        let splitbydot = splitbyline[j].split('.')
                        // idx first searchterm result of given line
                        for(let m = 0; m < splitbydot.length; ++m){
                            let o = 0
                            let n = 0
                            while(splitbydot[m].indexOf((lkw.split('.')[0].length>0?lkw.split('.')[0]:lkw.split('.')[1]),o) != -1){
                                // advance idx for context
                                contextIdx = currentdata.indexOf(splitdoc[contextIdxIdx])
                                contextIdxIdx++
                                //while the keyword split by dot(to match) found in sentence, split by space and find idx in word # in sentence
                                n = splitbydot[m].split(' ').indexOf((lkw.split('.')[0].length>0?(lkw.split('.')[0].split(' ').length>0?lkw.split('.')[0].split(' ')[0]:lkw.split('.')[0].split(' ')[1]):(lkw.split('.')[1].split(' ')[0].length>0?lkw.split('.')[1].split(' ')[0]:lkw.split('.')[1].split(' ')[1])),n)
                                // term will be n+1 word in m+1 sentence of j+1 paragraph of doc; context is string sliced around term
                                löysinJotain.push({
                                    'fileName':`${parentDir}${filesInDir[i]}`,
                                    'wordNumber':`${n==0?'1st':(n==1?'2nd':(n==2?'3rd':`${n+1}th`))}`,
                                    'sentenNum': `${m==0?'1st':(m==1?'2nd':(m==2?'3rd':`${m+1}th`))}`,
                                    'lineNumber':`${j==0?'1st':(j==1?'2nd':(j==2?'3rd':`${j+1}th`))}`,
                                    'lkwContext': ((((currentdata.indexOf(lkw,contextIdx) - 90) >= 0) && ((currentdata.indexOf(lkw,contextIdx)+lkw.length+90) <= currentdata.length - 1))?
                                        originalData.slice((currentdata.indexOf(lkw,contextIdx)-90), (currentdata.indexOf(lkw,contextIdx)+lkw.length+90)):
                                        (currentdata.indexOf(lkw,contextIdx) - 60 >= 0 && currentdata.indexOf(lkw,contextIdx)+lkw.length+60 <= currentdata.length - 1)?
                                        originalData.slice(currentdata.indexOf(lkw,contextIdx)-60, currentdata.indexOf(lkw,contextIdx)+lkw.length+60):
                                        (currentdata.indexOf(lkw,contextIdx) - 30 >= 0 && currentdata.indexOf(lkw,contextIdx)+lkw.length+30 <= currentdata.length - 1)?
                                        originalData.slice(currentdata.indexOf(lkw,contextIdx)-30, currentdata.indexOf(lkw,contextIdx)+lkw.length+30):
                                        (currentdata.indexOf(lkw,contextIdx) - 15 >= 0 && currentdata.indexOf(lkw,contextIdx)+lkw.length+15 <= currentdata.length - 1)?
                                        originalData.slice(currentdata.indexOf(lkw,contextIdx)-15, currentdata.indexOf(lkw,contextIdx)+lkw.length+15):
                                        (currentdata.indexOf(lkw,contextIdx) - 5 >= 0 && currentdata.indexOf(lkw,contextIdx)+lkw.length+5 <= currentdata.length - 1)?
                                        originalData.slice(currentdata.indexOf(lkw,contextIdx)-5, currentdata.indexOf(lkw,contextIdx)+lkw.length+5):
                                        (currentdata.indexOf(lkw,contextIdx) >= 0 && currentdata.indexOf(lkw,contextIdx)+lkw.length+15 <= currentdata.length - 1)?
                                        originalData.slice(currentdata.indexOf(lkw,contextIdx), currentdata.indexOf(lkw,contextIdx)+lkw.length+15):
                                        (currentdata.indexOf(lkw,contextIdx) - 15 >= 0 && currentdata.indexOf(lkw,contextIdx)+lkw.length <= currentdata.length - 1)?
                                        originalData.slice(currentdata.indexOf(lkw,contextIdx)-15, currentdata.indexOf(lkw,contextIdx)+lkw.length):kw)
                                })
                                o = 1 + splitbyline[j].split('.')[m].indexOf((lkw.split('.')[0].length>0?lkw.split('.')[0]:lkw.split('.')[1]),o)
                                n += 1
                                console.log('Keyword Located!')
                            }
                        }
                    }
                }
            }
        }
        // 一百%
        console.log('\n---------------------------------------------------\n\ndeep-file-search found ' + löysinJotain.length + ' instances of string: "' + kw + '".\n\n---------------------------------------------------')
        // Mot för Dator
        return löysinJotain

    } catch (err){
        // Ошибка / Rechnungsfehler
        console.log('Ошибка: ', err)
    }
}



// Run manually from cli 'node file-search.js', npm, or npx
if(parentDir.length>0){
    let locator = main().then((located)=>{
        let resultsFile = `Search Located ${located.length} keyword instances.\n\n${located.length>0?located.map((result, idx) => {return `\n-------------------- Result ${idx+1}: --------------------\n\n\t${result.fileName}\n\n\t${result.wordNumber} word in ${result.sentenNum} sentence on ${result.lineNumber} new line.\n\n${result.lkwContext}\n\n---------------------------------------------------\n`}):'No Results Found'}`
        if(saving==='true'){console.log(`\nOutput Written to File: \tdeep-file-search-${d.toISOString().split(':').join('-')}.txt\n`)}else{console.log('\nFile Not Saved. Results below:\n')}
        if(saving==='true'){fs.writeFileSync(`${outputDir}deep-file-search-${d.toISOString().split(':').join('-')}.txt`, (resultsFile+'\n\n\n'+JSON.stringify(located)))}else{console.log(resultsFile)}
    }).catch(err => {console.log('Ошибка:', err)})
}



// For running as require'd npm module
module.exports.corre = function (dir, keyword, enc = 'utf8', outputFile = 'false', outdir = '.'){
    parentDir = dir
    kw = keyword
    encode = enc
    saving = outputFile
    outputDir = outdir
    const locater = main().then((locate)=>{
        let resultsFile = `Search Located ${locate.length} keyword instances.\n\n${locate.length>0?locate.map((result, idx) => {return `\n-------------------- Result ${idx+1}: --------------------\n\n\t${result.fileName}\n\n\t${result.wordNumber} word in ${result.sentenNum} sentence on ${result.lineNumber} new line.\n\n${result.lkwContext}\n\n---------------------------------------------------\n`}):'No Results Found'}`
        if(outputFile==='true'||outputFile===true){fs.writeFileSync(`${outdir}deep-file-search-${d.toISOString().split(':').join('-')}.txt`, (resultsFile+'\n\n\n'+JSON.stringify(locate)))}else{console.log('\nFile Not Saved.\n')}
        console.log(`Search for ${kw} Completed with ${locate.length} results. Exiting Program.`)
        return {'located':locate, 'resultString':resultsFile}
    }).catch(err => console.log('Ошибка:', err))
    return locater
}



/* 

deep-file-search is a case-insensitive package which opens and scours all files in a given directory for a given search term and logs, returns or writes a new file containing human-readable and machine-parsable results indicating the locations of all instances of the particular search terms

Command Line Instruction:
0. Prepare arguments: place arguments in '' or "" (minus the <> below); Parent search dir & output dir need trailing "\"; If selecting output dir, encodeType and saveFile must be declared, even if defaults maintained ("utf8" "true")
1. npx deep-file-search search "<.\relative path to search dir\>" "<search term(s)>" "<encodeType(optional, default utf8)>" "<saveFile(optional, default true)>" "<.\outputDirectory(optional, must already exist, default outputs at terminal location)\>"
2. ???
3. Profit

// Programmatic Usage:
1. npm i deep-file-search
2. let deepFileSearch = require('deep-file-search')
3. let deepFileSearchResultsObject = await deep-file-search.corre(string_searchDir, string_searchTerm, OPTIONAL_fileEncodeType, OPTIONAL_bool_outputFile, OPTIONAL_string_outputDir) 

In Programmatic Usage, the default setting is not to output any file search results to disk.

File Encode Type (default is utf8); Options Include:
ascii, base64, base64url, binary, hex, latin1, ucs-2, ucs2, utf-8, utf16le, utf8


Thank You for Using deep-file-search, written by C. Alex @ YHY/SPC LLC 
We Contract Internationally!  🇷🇺 - 🇲🇽 - 🇨🇿 - 🇫🇮 - 🇸🇪 - 🇩🇪 - 🇺🇸 
Visit YHY.fi & work with YHY/SPC LLC on your next project.

*/